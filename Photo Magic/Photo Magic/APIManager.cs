﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Drawing;

namespace Photo_Magic
{
    class APIManager
    {
        private const String SERVER_URL = "http://10.0.203.2:3000";
        private static volatile APIManager instance;
        private static object syncRoot = new Object();
        private APIManager() {}

        public static APIManager SharedInstance
       {
          get 
          {
             if (instance == null) 
             {
                lock (syncRoot) 
                {
                   if (instance == null)
                       instance = new APIManager();
                }
             }

             return instance;
          }
       }
        public String Token(String email, String password)
        {
            String token = "";
            String tokenRequestUrl = SERVER_URL + "/token.json";
            String requestData =  "email=" + email + "&password=" + password;
            string responseStr = this.POST(tokenRequestUrl, requestData);
            Dictionary<string, string> response = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseStr);
            if (response.ContainsKey("token"))
            {
                token = response["token"];
            }
            else
            {
                throw new Exception(response["message"]);
            }
            return token;
        }
        public List<ImageRecord> LoadPhotos(string email, string token)
        {
            String photosRequestUrl = SERVER_URL + "/public.json";
            String res = this.GET(photosRequestUrl, "user_email=" + email + "&user_token=" + token);
            List<ImageRecord> images = JsonConvert.DeserializeObject<List<ImageRecord>>(res);
            return images;
        }

        public Image ImageWithUrl(String imageUrl)
        {
            Image tmpimg = null;
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(SERVER_URL+"/"+imageUrl);
            HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream stream = httpWebReponse.GetResponseStream();
            return Image.FromStream(stream);
        }
        private string JsonFromResponse(HttpWebResponse res)
        {
            string Out = String.Empty;
            Stream ReceiveStream = res.GetResponseStream();
            StreamReader sr = new StreamReader(ReceiveStream, Encoding.UTF8);
            //Кодировка указывается в зависимости от кодировки ответа сервера
            Char[] read = new Char[256];
            int count = sr.Read(read, 0, 256);
            while (count > 0)
            {
                String str = new String(read, 0, count);
                Out += str;
                count = sr.Read(read, 0, 256);
            }
            return Out;
        }

        private string GET(string Url, string Data)
        {
            WebRequest req = WebRequest.Create(Url + "?" + Data);
            return this.JsonFromResponse(req.GetResponse() as HttpWebResponse);
        }
        private string POST(string Url, string Data)
        {
            String outData;
            HttpWebRequest req = WebRequest.Create(Url) as HttpWebRequest;
            req.Method = "POST";
            req.Timeout = 100000;
            byte[] sentData = Encoding.GetEncoding(1251).GetBytes(Data);
            req.ContentLength = sentData.Length;
            Stream sendStream = req.GetRequestStream();
            sendStream.Write(sentData, 0, sentData.Length);
            sendStream.Close();
            try
            {
                HttpWebResponse res = req.GetResponse() as HttpWebResponse;
                outData = this.JsonFromResponse(res);
            }
            catch (WebException e)
            {
                outData = this.JsonFromResponse(e.Response as HttpWebResponse);
            }

            return outData;
        }
    }
}
