﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Photo_Magic
{
    public partial class Form1 : Form
    {
        private LoginForm _loginForm;
        private PhotoMagic _photoMagic;
        public Form1()
        {
            InitializeComponent();
            _loginForm = new LoginForm();
            _photoMagic = new PhotoMagic();
            _loginForm.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<ImageRecord> photos = APIManager.SharedInstance.LoadPhotos("test@gmail.com", "YuwEs-FKgjn4himyuSFz");
            int i = 0;
            foreach (ImageRecord photo in photos)
            {
                String imageName = "Image " + i;
                i++;
                Image image = APIManager.SharedInstance.ImageWithUrl(photo.photo_url);
                listView1.Items.Add(photo.description,i);
                imageList1.Images.Add(imageName,image);
            }
            listView1.LargeImageList = imageList1;

  
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
