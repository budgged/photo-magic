﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Photo_Magic
{
    public partial class LoginForm : Form
    {
        private String token;
        private String email;
        public LoginForm()
        {
            InitializeComponent();
            mainPanel.Hide();

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            emailField.Text = "test@gmail.com";
            passwordField.Text = "12345678";
            passwordField.PasswordChar = '*';
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                String token = APIManager.SharedInstance.Token(emailField.Text, passwordField.Text);
                Console.WriteLine("Token: " + token);
                this.email = emailField.Text;
                this.token = token;
                this.LoadImages(this.email, this.token);
                loginPanel.Hide();
                mainPanel.Show();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                MessageBox.Show(exc.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void LoadImages(String email, String token) {
            List<ImageRecord> photos = APIManager.SharedInstance.LoadPhotos("test@gmail.com", "YuwEs-FKgjn4himyuSFz");
            int i = 0;
            imageList1.Images.Clear();
            listView1.Items.Clear();
            foreach (ImageRecord photo in photos)
            {
                String imageName = "Image " + i;
                Image image = APIManager.SharedInstance.ImageWithUrl(photo.photo_url);
                image.Save("./Image " + i + ".jpg");
                imageList1.Images.Add(image);
                listView1.Items.Add(photo.description, i);
                i++;


            }
            listView1.LargeImageList = imageList1;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void update_Click(object sender, EventArgs e)
        {
            this.LoadImages(this.email, this.token);
        }
    }
}
