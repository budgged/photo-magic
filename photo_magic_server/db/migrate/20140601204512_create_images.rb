class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :description, default: ""

      t.timestamps
    end
  end
end
