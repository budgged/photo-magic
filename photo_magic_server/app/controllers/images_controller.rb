class ImagesController < ApplicationController
  def index
    @images = Image.all
  end

  def new 
    @image = Image.new
  end

  def create
    @image = Image.create(image_params)
    @images = Image.all
    render 'index'
  end

  def destroy
    @image = Image.find(image_params)
    @image.destroy
  end

  private 
    def image_params
      params.require(:image).permit(:uploaded_image, :description)
    end
end