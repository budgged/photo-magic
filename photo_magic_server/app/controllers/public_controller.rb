class PublicController < ApplicationController
  acts_as_token_authentication_handler_for User
  #before_action :authenticate_user!
  def index
    @images = Image.all
    responde = @images.map {|i| {description: i.description, photo_url: i.uploaded_image.url}}
    respond_to do |format|
      format.html
      format.json {render json: responde, status: :ok}
    end
  end
end