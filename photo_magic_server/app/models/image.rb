class Image < ActiveRecord::Base
  has_attached_file :uploaded_image,:default_url => "/system/images/uploaded_images/missing.png"
  validates_attachment_content_type :uploaded_image, :content_type => /\Aimage\/.*\Z/
end
